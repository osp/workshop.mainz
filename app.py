from flask import Flask, render_template
import get_wikipedia_page

app = Flask(__name__)


# a list of all the "saved" pages
saved_requests = ['Virus', 'Algae', 'Waterfall', 'Seashell', 'Cloud']

# 'Mouse', 'Ether', 'Pipeline transport', 'Python (genus)',


# the index show the current saved pages
@app.route("/")
def index():
    return render_template(
        'index.html',
        saved_requests = saved_requests)


# when we request any page that is not the index,
# it does a wiki request for that string
@app.route("/<request>--get")
def page(request):

    print("here is what has been requested: " + request)

    # get page
    page = get_wikipedia_page.get_page(request)

    # render templates
    return render_template(
        'page.html',
        page = page)


# in order to be sure that anything written in a pad can't influence/break the interface
# we separate the inferface from the raw rendering through another layer of iframes
@app.route("/<request>")
def page_interface(request):

    # get page
    page = get_wikipedia_page.get_page(request)

    # render templates
    return render_template(
        'page-interface.html',
        page = page)


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')